﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LogEvents : MonoBehaviour {

    public Text m_logText;

	// Use this for initialization
	void Start () {
	
	}

    //public string output = "";
    //public string stack = "";

    void OnEnable()
    {
        Application.logMessageReceived += HandleLog;
    }

    void OnDisable()
    {
        Application.logMessageReceived -= HandleLog;
    }

    /// <summary>
    /// Clear the Log Text
    /// </summary>
    public void clearLog()
    {
        m_logText.text = "";
    }

    void HandleLog(string logString, string stackTrace, LogType type)
    {
        //output = logString;
        //stack = stackTrace;

        string newLog;

        if (type == LogType.Log)
        {
            //do nothing
            newLog = "Log: " + logString;
        }
        else if(type == LogType.Warning)
        {
            newLog = "<color=yellow>Warning: </color>" + logString;
        }
        else if(type == LogType.Error)
        {
            newLog = "<color=red>Error: </color>" + logString;
        }
        else
        {
            //unknown type?
            newLog = "Log: " + logString;
        }

        addLogChat(newLog);

    }

    private void addLogChat(string log)
    {
        m_logText.text += "\n " + log;
    }

}
