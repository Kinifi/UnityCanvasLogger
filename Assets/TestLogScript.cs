﻿using UnityEngine;
using System.Collections;

public class TestLogScript : MonoBehaviour {

	// Use this for initialization
	void Start () {

        StartCoroutine(WaitAndPrint());
	
	}

    private IEnumerator WaitAndPrint()
    {
        // suspend execution for 5 seconds
        Debug.Log("This is a Log");
        yield return new WaitForSeconds(5);
        Debug.LogWarning("This is a warning");
        yield return new WaitForSeconds(5);
        Debug.LogError("This is a Error");
    }
}
